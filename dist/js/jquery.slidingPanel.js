(function($) {

	$.fn.slidingPanel = function(options) {
		var settings = $.extend({
			// Options
			addBackgroundOverlay: true, // If an overlay needs to be appended to the body | Options: Boolean (true / false)
			addCloseButton: true, // If a close button needs to be appended to the sliding panel | Options: Boolean (true / false)
			appearFrom: 'left', // Direction from which the sliding panel appears | Options: 'left', 'right'
			btnOpenSlidingPanel: '.btn-open-sliding-panel', // jQuery selector that opens the sliding panel | Options: String
			closeButtonContent: '', // Markup to add inside the close button | Options: String
			closeOnEsc: true, // If the sliding panel must close on the esc keyup event | Options: Boolean (true / false)
			closeOnOverlay: true, // If the sliding panel must close on the overlay click event | Options: Boolean (true / false)
			closeOnSwipe: true, // If the sliding panel must close on the swipe event | Options: Boolean (true / false)
			contentToClone: null, // jQuery object to clone and append in the sliding panel | Options: jQuery object, null
			transitionDurationOnOpen: 200, // CSS transition duration when the sliding panel opens (in ms) | Options: Integer
			transitionDurationOnClose: 200, // CSS transition duration when the sliding panel closes (in ms) | Options: Integer

			// Callbacks
			onOpenBefore: function() {},
			onOpenAfter: function() {},
			onCloseBefore: function() {},
			onCloseAfter: function() {}
		}, options);

		// Public method opening the sliding panel
		this.open = function() {
			openSlidingPanel();
		};

		// Public method closing the sliding panel
		this.close = function() {
			closeSlidingPanel();
		};

		var $body = $('body'),
			$siteWrapper = $('#site-wrapper'),
			$btnOpenSlidingPanel = $(settings.btnOpenSlidingPanel),
			$slidingPanel = this,
			slidingPanelId = $slidingPanel.attr('id'),
			$slidingPanelContent = $slidingPanel.find('.sliding-panel-overflow');

		// Add an ID to the sliding panel scrolling div
		$slidingPanelContent.attr('id', slidingPanelId + '-overflow');

		// Append a close button if set to true
		if(settings.addCloseButton) {
			$slidingPanel.append('<button type="button" class="btn btn-default btn-close-' + slidingPanelId + '">' + settings.closeButtonContent + '</button>');
		}

		// Append navigation content if it is specified
		if(settings.contentToClone) {
			$slidingPanelContent.append(settings.contentToClone.clone());
		}

		// Add appearing direction class
		if(settings.appearFrom !== '') {
			if(settings.appearFrom === 'right') {
				$slidingPanel.addClass('appear-from-right');
			} else if(settings.appearFrom === 'left') {
				$slidingPanel.addClass('appear-from-left');
			}
		}

		// Append the background overlay div if it is specified
		if(settings.addBackgroundOverlay && !$('#' + slidingPanelId + '-overlay').length) {
			$siteWrapper.append('<div id="' + slidingPanelId + '-overlay"><br></div>');
		}

		// Verify if device is touch
		function isTouchDevice() {
			try {
				document.createEvent("TouchEvent");
				
				return true;
			} catch(e) {
				return false;
			}
		}

		if(isTouchDevice()) { //if touch events exist...
			var scrollStartPos = 0,
				element = document.getElementById(slidingPanelId + '-overflow');

			element.addEventListener("touchstart", function(event) {
				scrollStartPos = this.scrollTop + event.touches[0].pageY;
			}, false);

			element.addEventListener("touchmove", function(event) {
				this.scrollTop = scrollStartPos - event.touches[0].pageY;
			}, false);
		}

		function openSlidingPanel() {
			if($body.hasClass(slidingPanelId + '-is-animating')) {
				return false;
			}

			if (typeof settings.onOpenBefore === 'function') { // make sure the callback is a function
				settings.onOpenBefore.call(this); // brings the scope to the callback
			}

			$body.addClass(slidingPanelId + '-is-animating');

			$btnOpenSlidingPanel.attr('aria-expanded', 'true');

			setTimeout(function() {
				$body.addClass(slidingPanelId + '-is-animating-in');
			}, 10);

			setTimeout(function() {
				$body.removeClass(slidingPanelId + '-is-animating ' + slidingPanelId + '-is-animating-in').addClass(slidingPanelId + '-is-open');

				if (typeof settings.onOpenAfter === 'function') { // make sure the callback is a function
					settings.onOpenAfter.call(this); // brings the scope to the callback
				}
			}, settings.transitionDurationOnOpen);
		}

		function closeSlidingPanel() {
			if($body.hasClass(slidingPanelId + '-is-animating')) {
				return false;
			}

			if (typeof settings.onCloseBefore === 'function') { // make sure the callback is a function
				settings.onCloseBefore.call(this); // brings the scope to the callback
			}

			$body.removeClass(slidingPanelId + '-is-open').addClass(slidingPanelId + '-is-animating ' + slidingPanelId + '-is-animating-out');

			$btnOpenSlidingPanel.attr('aria-expanded', 'false');

			setTimeout(function() {
				$body.removeClass(slidingPanelId + '-is-animating ' + slidingPanelId + '-is-animating-out');


				if (typeof settings.onCloseAfter === 'function') { // make sure the callback is a function
					settings.onCloseAfter.call(this); // brings the scope to the callback
				}
			}, settings.transitionDurationOnClose);
		}

		// Open sliding panel on .btn-open-sliding-panel click and touchstart events
		$body.on('click touchstart', settings.btnOpenSlidingPanel, function(e) {
			e.stopPropagation();

			if (!$(settings.btnOpenSlidingPanel).is(':button')) {
				e.preventDefault();
			}

			if(e.handled !== true  && !$body.hasClass(slidingPanelId + '-is-open')) {
				openSlidingPanel();

				e.handled = true;
			} else {
				return false;
			}
		});

		// Close sliding panel on .btn-close-sliding-panel click and touchstart events
		$body.on('click touchstart', '.btn-close-' + slidingPanelId, function(e) {
			e.stopPropagation();

			if (!$('.btn-close-' + slidingPanelId).is(':button')) {
				e.preventDefault();
			}

			if(e.handled !== true && $body.hasClass(slidingPanelId + '-is-open')) {
				closeSlidingPanel();

				e.handled = true;
			} else {
				return false;
			}
		});

		// Close sliding panel on #sliding-panel-overlay click and touchstart events if closeOnOverlay is set to true
		if(settings.closeOnOverlay) {
			$body.on('click touchstart', '#' + slidingPanelId + '-overlay', function(e) {
				e.stopPropagation();

				if(e.handled !== true && $body.hasClass(slidingPanelId + '-is-open')) {
					closeSlidingPanel();

					e.handled = true;
				} else {
					return false;
				}
			});
		}

		// Close on ESC key if closeOnEsc is set to true
		if(settings.closeOnEsc) {
			$(document).keyup(function(e) {
				if(e.keyCode === 27 && $body.hasClass(slidingPanelId + '-is-open')) {
					closeSlidingPanel();
				}
			});
		}

		// Close on swipe if closeOnSwipe is specified
		if(settings.closeOnSwipe) {
			$slidingPanel.swipe( {
				//Generic swipe handler for all directions
				swipe: function(event, direction) {
					if($body.hasClass(slidingPanelId + '-is-open')) {
						if(settings.appearFrom === 'right' && direction === 'right') {
							closeSlidingPanel();
						} else if(settings.appearFrom === 'left' && direction === 'left') {
							closeSlidingPanel();
						}
					}
				},
				preventDefaultEvents: false
			});
		}

		return this;
	};

}(jQuery));